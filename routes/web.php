<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=> 'web'],function(){
});
//publicity Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('publicity','\App\Http\Controllers\PublicityController');
  Route::post('publicity/{id}/update','\App\Http\Controllers\PublicityController@update');
  Route::get('publicity/{id}/delete','\App\Http\Controllers\PublicityController@destroy');
  Route::get('publicity/{id}/deleteMsg','\App\Http\Controllers\PublicityController@DeleteMsg');
});
