@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content')

<section class="content">
    <h1>
        Create publicity
    </h1>
    <a href="{!!url('publicity')!!}" class = 'btn btn-danger'><i class="fa fa-home"></i> Publicity Index</a>
    <br>
    <form method = 'POST' action = '{!!url("publicity")!!}'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="name">name</label>
            <input required id="name" name = "name" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <input required id="description" name = "description" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="message">message</label>
            <input required id="message" name = "message" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="background_route">background_route</label>
            <input required id="background_route" name = "background_route" type="file" class="form-control">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">User select</label>
            <select required class="form-control" id="exampleFormControlSelect1">
                <option> Select a user </option>
                @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
        </div>
        <button class = 'btn btn-success' type ='submit'> <i class="fa fa-floppy-o"></i> Save</button>
    </form>
</section>
@endsection