@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        Show publicity
    </h1>
    <br>
    <a href='{!!url("publicity")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i>Publicity Index</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$publicity->name!!}</td>
            </tr>
            <tr>
                <td> <b>description</b> </td>
                <td>{!!$publicity->description!!}</td>
            </tr>
            <tr>
                <td> <b>message</b> </td>
                <td>{!!$publicity->message!!}</td>
            </tr>
            <tr>
                <td> <b>background_route</b> </td>
                <td>{!!$publicity->background_route!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection