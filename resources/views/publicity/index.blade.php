@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        Publicity Index
    </h1>
    <a href='{!!url("publicity")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> New</a>
    <br>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>name</th>
            <th>description</th>
            <th>message</th>
            <th>background_route</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($publicities as $publicity) 
            <tr>
                <td>{!!$publicity->name!!}</td>
                <td>{!!$publicity->description!!}</td>
                <td>{!!$publicity->message!!}</td>
                <td>{!!$publicity->background_route!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/publicity/{!!$publicity->id!!}/deleteMsg" ><i class = 'fa fa-trash'> delete</i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/publicity/{!!$publicity->id!!}/edit'><i class = 'fa fa-edit'> edit</i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/publicity/{!!$publicity->id!!}'><i class = 'fa fa-eye'> info</i></a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $publicities->render() !!}

</section>
@endsection