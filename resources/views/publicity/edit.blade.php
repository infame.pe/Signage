@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        Edit publicity
    </h1>
    <a href="{!!url('publicity')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> Publicity Index</a>
    <br>
    <form method = 'POST' action = '{!! url("publicity")!!}/{!!$publicity->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="name">name</label>
            <input id="name" name = "name" type="text" class="form-control" value="{!!$publicity->
            name!!}"> 
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <input id="description" name = "description" type="text" class="form-control" value="{!!$publicity->
            description!!}"> 
        </div>
        <div class="form-group">
            <label for="message">message</label>
            <input id="message" name = "message" type="text" class="form-control" value="{!!$publicity->
            message!!}"> 
        </div>
        <div class="form-group">
            <label for="background_route">background_route</label>
            <input id="background_route" name = "background_route" type="file" class="form-control" value="{!!$publicity->
            background_route!!}"> 
        </div>
        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> Update</button>
    </form>
</section>
@endsection