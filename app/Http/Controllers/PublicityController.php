<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Publicity;
use Amranidev\Ajaxis\Ajaxis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use URL;

/**
 * Class PublicityController.
 *
 * @author  The scaffold-interface created at 2018-06-02 09:12:24am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class PublicityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - publicity';
        $publicities = Publicity::paginate(6);
        return view('publicity.index',compact('publicities','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - publicity';
        $users = User::all();
        
        return view('publicity.create',compact('users','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $publicity = new Publicity();

        
        $publicity->name = $request->name;

        
        $publicity->description = $request->description;

        
        $publicity->message = $request->message;

        
        $publicity->background_route = $request->background_route;

        
        $publicity->save();

        $user = Auth::user();

        $relation = ['user_id' => $user->id, 'publicity_id' => $publicity->id];

        DB::table('publicity_user')->insert($relation);

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new publicity has been created !!']);

        return redirect('publicity');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - publicity';

        if($request->ajax())
        {
            return URL::to('publicity/'.$id);
        }

        $publicity = Publicity::findOrfail($id);
        return view('publicity.show',compact('title','publicity'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - publicity';
        if($request->ajax())
        {
            return URL::to('publicity/'. $id . '/edit');
        }

        
        $publicity = Publicity::findOrfail($id);
        return view('publicity.edit',compact('title','publicity'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $publicity = Publicity::findOrfail($id);
    	
        $publicity->name = $request->name;
        
        $publicity->description = $request->description;
        
        $publicity->message = $request->message;
        
        $publicity->background_route = $request->background_route;
        
        
        $publicity->save();

        return redirect('publicity');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Warning!!','Would you like to remove This?','/publicity/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$publicity = Publicity::findOrfail($id);
     	$publicity->delete();
        return URL::to('publicity');
    }
}
