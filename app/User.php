<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


	/**
     * publicity.
     *
     * @return  \Illuminate\Support\Collection;
     */
    public function publicities()
    {
        return $this->belongsToMany('App\Publicity');
    }

    /**
     * Assign a publicity.
     *
     * @param  $publicity
     * @return  mixed
     */
    public function assignPublicity($publicity)
    {
        return $this->publicities()->attach($publicity);
    }
    /**
     * Remove a publicity.
     *
     * @param  $publicity
     * @return  mixed
     */
    public function removePublicity($publicity)
    {
        return $this->publicities()->detach($publicity);
    }

}
